FROM cloudron/base:0.10.0
MAINTAINER Johannes Zellner <johannes@cloudron.io>

RUN mkdir -p /app/code
WORKDIR /app/code

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "/dev/stderr",' -i /etc/apache2/apache2.conf
RUN sed -e "s,MaxSpareServers[^:].*,MaxSpareServers 5," -i /etc/apache2/mods-available/mpm_prefork.conf
RUN a2disconf other-vhosts-access-log
ADD apache2-shaarli.conf /etc/apache2/sites-available/shaarli.conf
RUN ln -sf /etc/apache2/sites-available/shaarli.conf /etc/apache2/sites-enabled/shaarli.conf
RUN a2enmod php7.0
RUN a2enmod rewrite

# configure mod_php
RUN crudini --set /etc/php/7.0/apache2/php.ini PHP upload_max_filesize 8M && \
    crudini --set /etc/php/7.0/apache2/php.ini PHP post_max_size 8M && \
    crudini --set /etc/php/7.0/apache2/php.ini PHP memory_limit 128M && \
    crudini --set /etc/php/7.0/apache2/php.ini Session session.save_path /run/shaarli/sessions && \
    crudini --set /etc/php/7.0/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.0/apache2/php.ini Session session.gc_divisor 100

# use shaarli from source (https://github.com/shaarli/Shaarli/wiki/Download-and-Installation) because of #874
# RUN curl -L https://github.com/shaarli/Shaarli/releases/download/v0.9.0/shaarli-v0.9.0-full.tar.gz | tar -xz --strip-components 1 -f -
RUN curl -L https://github.com/shaarli/Shaarli/archive/61c15aa5554431893ea5ebe800a9a625dca5aff9.tar.gz | tar -xz --strip-components 1 -f -
RUN php /usr/local/bin/composer install --no-dev --prefer-dist --optimize-autoloader

# these links will become valid after setup is run
RUN mv /app/code/data /app/code/data.orig && ln -sf /app/data/data /app/code/data && \
    mv /app/code/tpl /app/code/tpl.orig && ln -sf /app/data/tpl /app/code/tpl && \
    mv /app/code/plugins /app/code/plugins.orig && ln -sf /app/data/plugins/ /app/code/plugins && \
    rm -rf /app/code/cache && ln -sf /run/shaarli/cache /app/code/cache && \
    rm -rf /app/code/pagecache && ln -sf /run/shaarli/pagecache /app/code/pagecache && \
    rm -rf /app/code/tmp && ln -sf /run/shaarli/tmp /app/code/tmp

RUN chown -R www-data:www-data /app/code

COPY start.sh /app/code/

CMD [ "/app/code/start.sh" ]
