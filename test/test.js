#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    webdriver = require('selenium-webdriver');

var by = require('selenium-webdriver').By,
    until = require('selenium-webdriver').until;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

describe('Application life cycle test', function () {
    this.timeout(0);

    var chrome = require('selenium-webdriver/chrome');
    var server, browser = new chrome.Driver();

    var TEST_TIMEOUT = 30000;
    var LOCATION = 'test';

    var app;
    var username = 'admin';
    var password = 'changeme';
    var title = 'My Bookmarks';
    var testLink = 'https://cloudron.io';
    var testDescription = 'Run for it';

    before(function (done) {
        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        done();
    });

    function waitForElement(elem) {
        return new Promise(function (resolve, reject) {
            browser.wait(until.elementLocated(elem), TEST_TIMEOUT).then(function () {
                resolve(browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT));
            });
        });
    }

    function login(done) {
        browser.get('https://' + app.fqdn + '/?do=login').then(function () {
            return waitForElement(by.xpath('//div[@id="login-form"]//input[@name="login"]'));
        }).then(function () {
            return browser.findElement(by.xpath('//div[@id="login-form"]//input[@name="login"]')).sendKeys(username);
        }).then(function () {
            return browser.findElement(by.xpath('//div[@id="login-form"]//input[@name="password"]')).sendKeys(password);
        }).then(function () {
            return browser.findElement(by.xpath('//div[@id="login-form"]//input[@type="submit"]')).click();
        }).then(function () {
            return waitForElement(by.xpath('//a[text()="Tools"]'));
        }).then(function () {
            done();
        });
    }

    function logout(done) {
        browser.get('https://' + app.fqdn + '/?do=logout').then(function () {
            return waitForElement(by.xpath('//a[@id="login-button"]'));
        }).then(function () {
            done();
        });
    }

    function addLink(done) {
        browser.get('https://' + app.fqdn +'/?do=addlink').then(function () {
            return waitForElement(by.xpath('//input[@name="post"]'));
        }).then(function () {
            return browser.findElement(by.xpath('//input[@name="post"]')).sendKeys(testLink);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@type="submit"]')).click();
        }).then(function () {
            return waitForElement(by.xpath('//textarea[@name="lf_description"]'));
        }).then(function () {
            return browser.findElement(by.xpath('//textarea[@name="lf_description"]')).sendKeys(testDescription);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@name="save_edit"]')).click();
        }).then(function () {
            return waitForElement(by.xpath('//a[@href="' + testLink + '"]'));
        }).then(function () {
            done();
        });
    }

    function linkExists(done) {
        browser.get('https://' + app.fqdn +'/?do=daily').then(function () {
            return waitForElement(by.xpath('//div[text()="' + testDescription + '"]'));
        }).then(function () {
            done();
        });
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app', function () {
        execSync('cloudron install --new --wait --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can complete install', function (done) {
        browser.get('https://' + app.fqdn).then(function () {
            return waitForElement(by.xpath('//input[@name="setlogin"]'));
        }).then(function () {
            return browser.findElement(by.xpath('//input[@name="setlogin"]')).sendKeys(username);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@name="setpassword"]')).sendKeys(password);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@name="title"]')).sendKeys(title);
        }).then(function () {
            return browser.findElement(by.xpath('//input[@name="Save"]')).click();
        }).then(function () {
            var alert = browser.switchTo().alert();
            return alert.accept();
        }).then(function () {
            done();
        });
    });

    it('can login', login);
    it('can add link', addLink);
    it('link exists', linkExists);
    it('can logout', logout);

    it('can restart app', function (done) {
        execSync('cloudron restart --app ' + app.id);
        done();
    });

    it('can login', login);
    it('link exists', linkExists);
    it('can logout', logout);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can login', login);
    it('link exists', linkExists);
    it('can logout', logout);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --wait --location ' + LOCATION + '2 --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can login', login);
    it('link exists', linkExists);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});
